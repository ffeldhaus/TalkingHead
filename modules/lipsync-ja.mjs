class LipsyncJa {
  /**
   * @constructor
   */
  constructor() {
    // Japanese words to Oculus visemes
    // Based on general pronunciation rules and common phoneme-viseme mappings
    this.rules = {
      // Hiragana
      あ: [{ regex: /あ/g, visemes: ["aa"], move: 1 }],
      い: [{ regex: /い/g, visemes: ["I"], move: 1 }],
      う: [{ regex: /う/g, visemes: ["U"], move: 1 }],
      え: [{ regex: /え/g, visemes: ["E"], move: 1 }],
      お: [{ regex: /お/g, visemes: ["O"], move: 1 }],
      か: [{ regex: /か/g, visemes: ["kk", "aa"], move: 1 }],
      き: [{ regex: /き/g, visemes: ["kk", "I"], move: 1 }],
      く: [{ regex: /く/g, visemes: ["kk", "U"], move: 1 }],
      け: [{ regex: /け/g, visemes: ["kk", "E"], move: 1 }],
      こ: [{ regex: /こ/g, visemes: ["kk", "O"], move: 1 }],
      さ: [{ regex: /さ/g, visemes: ["SS", "aa"], move: 1 }],
      し: [{ regex: /し/g, visemes: ["SS", "I"], move: 1 }],
      す: [{ regex: /す/g, visemes: ["SS", "U"], move: 1 }],
      せ: [{ regex: /せ/g, visemes: ["SS", "E"], move: 1 }],
      そ: [{ regex: /そ/g, visemes: ["SS", "O"], move: 1 }],
      た: [{ regex: /た/g, visemes: ["DD", "aa"], move: 1 }],
      ち: [{ regex: /ち/g, visemes: ["CH", "I"], move: 1 }],
      つ: [{ regex: /つ/g, visemes: ["TS", "U"], move: 1 }],
      て: [{ regex: /て/g, visemes: ["DD", "E"], move: 1 }],
      と: [{ regex: /と/g, visemes: ["DD", "O"], move: 1 }],
      な: [{ regex: /な/g, visemes: ["nn", "aa"], move: 1 }],
      に: [{ regex: /に/g, visemes: ["nn", "I"], move: 1 }],
      ぬ: [{ regex: /ぬ/g, visemes: ["nn", "U"], move: 1 }],
      ね: [{ regex: /ね/g, visemes: ["nn", "E"], move: 1 }],
      の: [{ regex: /の/g, visemes: ["nn", "O"], move: 1 }],
      は: [{ regex: /は/g, visemes: ["HH", "aa"], move: 1 }],
      ひ: [{ regex: /ひ/g, visemes: ["HH", "I"], move: 1 }],
      ふ: [{ regex: /ふ/g, visemes: ["FF", "U"], move: 1 }],
      へ: [{ regex: /へ/g, visemes: ["HH", "E"], move: 1 }],
      ほ: [{ regex: /ほ/g, visemes: ["HH", "O"], move: 1 }],
      ま: [{ regex: /ま/g, visemes: ["PP", "aa"], move: 1 }],
      み: [{ regex: /み/g, visemes: ["PP", "I"], move: 1 }],
      む: [{ regex: /む/g, visemes: ["PP", "U"], move: 1 }],
      め: [{ regex: /め/g, visemes: ["PP", "E"], move: 1 }],
      も: [{ regex: /も/g, visemes: ["PP", "O"], move: 1 }],
      や: [{ regex: /や/g, visemes: ["jj", "aa"], move: 1 }],
      ゆ: [{ regex: /ゆ/g, visemes: ["jj", "U"], move: 1 }],
      よ: [{ regex: /よ/g, visemes: ["jj", "O"], move: 1 }],
      ら: [{ regex: /ら/g, visemes: ["RR", "aa"], move: 1 }],
      り: [{ regex: /り/g, visemes: ["RR", "I"], move: 1 }],
      る: [{ regex: /る/g, visemes: ["RR", "U"], move: 1 }],
      れ: [{ regex: /れ/g, visemes: ["RR", "E"], move: 1 }],
      ろ: [{ regex: /ろ/g, visemes: ["RR", "O"], move: 1 }],
      わ: [{ regex: /わ/g, visemes: ["ww", "aa"], move: 1 }],
      を: [{ regex: /を/g, visemes: ["O"], move: 1 }],
      ん: [{ regex: /ん/g, visemes: ["nn"], move: 1 }],

      // Katakana
      ア: [{ regex: /ア/g, visemes: ["aa"], move: 1 }],
      イ: [{ regex: /イ/g, visemes: ["I"], move: 1 }],
      ウ: [{ regex: /ウ/g, visemes: ["U"], move: 1 }],
      エ: [{ regex: /エ/g, visemes: ["E"], move: 1 }],
      オ: [{ regex: /オ/g, visemes: ["O"], move: 1 }],
      カ: [{ regex: /カ/g, visemes: ["kk", "aa"], move: 1 }],
      キ: [{ regex: /キ/g, visemes: ["kk", "I"], move: 1 }],
      ク: [{ regex: /ク/g, visemes: ["kk", "U"], move: 1 }],
      ケ: [{ regex: /ケ/g, visemes: ["kk", "E"], move: 1 }],
      コ: [{ regex: /コ/g, visemes: ["kk", "O"], move: 1 }],
      サ: [{ regex: /サ/g, visemes: ["SS", "aa"], move: 1 }],
      シ: [{ regex: /シ/g, visemes: ["SS", "I"], move: 1 }],
      ス: [{ regex: /ス/g, visemes: ["SS", "U"], move: 1 }],
      セ: [{ regex: /セ/g, visemes: ["SS", "E"], move: 1 }],
      ソ: [{ regex: /ソ/g, visemes: ["SS", "O"], move: 1 }],
      タ: [{ regex: /タ/g, visemes: ["DD", "aa"], move: 1 }],
      チ: [{ regex: /チ/g, visemes: ["CH", "I"], move: 1 }],
      ツ: [{ regex: /ツ/g, visemes: ["TS", "U"], move: 1 }],
      テ: [{ regex: /テ/g, visemes: ["DD", "E"], move: 1 }],
      ト: [{ regex: /ト/g, visemes: ["DD", "O"], move: 1 }],
      ナ: [{ regex: /ナ/g, visemes: ["nn", "aa"], move: 1 }],
      ニ: [{ regex: /ニ/g, visemes: ["nn", "I"], move: 1 }],
      ヌ: [{ regex: /ヌ/g, visemes: ["nn", "U"], move: 1 }],
      ネ: [{ regex: /ネ/g, visemes: ["nn", "E"], move: 1 }],
      ノ: [{ regex: /ノ/g, visemes: ["nn", "O"], move: 1 }],
      ハ: [{ regex: /ハ/g, visemes: ["HH", "aa"], move: 1 }],
      ヒ: [{ regex: /ヒ/g, visemes: ["HH", "I"], move: 1 }],
      フ: [{ regex: /フ/g, visemes: ["FF", "U"], move: 1 }],
      ヘ: [{ regex: /ヘ/g, visemes: ["HH", "E"], move: 1 }],
      ホ: [{ regex: /ホ/g, visemes: ["HH", "O"], move: 1 }],
      マ: [{ regex: /マ/g, visemes: ["PP", "aa"], move: 1 }],
      ミ: [{ regex: /ミ/g, visemes: ["PP", "I"], move: 1 }],
      ム: [{ regex: /ム/g, visemes: ["PP", "U"], move: 1 }],
      メ: [{ regex: /メ/g, visemes: ["PP", "E"], move: 1 }],
      モ: [{ regex: /モ/g, visemes: ["PP", "O"], move: 1 }],
      ヤ: [{ regex: /ヤ/g, visemes: ["jj", "aa"], move: 1 }],
      ユ: [{ regex: /ユ/g, visemes: ["jj", "U"], move: 1 }],
      ヨ: [{ regex: /ヨ/g, visemes: ["jj", "O"], move: 1 }],
      ラ: [{ regex: /ラ/g, visemes: ["RR", "aa"], move: 1 }],
      リ: [{ regex: /リ/g, visemes: ["RR", "I"], move: 1 }],
      ル: [{ regex: /ル/g, visemes: ["RR", "U"], move: 1 }],
      レ: [{ regex: /レ/g, visemes: ["RR", "E"], move: 1 }],
      ロ: [{ regex: /ロ/g, visemes: ["RR", "O"], move: 1 }],
      ワ: [{ regex: /ワ/g, visemes: ["ww", "aa"], move: 1 }],
      ヲ: [{ regex: /ヲ/g, visemes: ["OO"], move: 1 }],
      ヲ: [{ regex: /ヲ/g, visemes: ["O"], move: 1 }],
      ン: [{ regex: /ン/g, visemes: ["nn"], move: 1 }],

      // Common Kanji (add more as needed)
      一: [{ regex: /一/g, visemes: ["I", "CH"], move: 1 }],
      二: [{ regex: /二/g, visemes: ["nn", "I"], move: 1 }],
      三: [{ regex: /三/g, visemes: ["SS", "aa", "nn"], move: 1 }],
      四: [{ regex: /四/g, visemes: ["jj", "OO", "nn"], move: 1 }],
      五: [{ regex: /五/g, visemes: ["kk", "O"], move: 1 }],
      六: [{ regex: /六/g, visemes: ["RR", "OO", "kk", "U"], move: 1 }],
      七: [{ regex: /七/g, visemes: ["nn", "aa", "nn", "aa"], move: 1 }],
      八: [{ regex: /八/g, visemes: ["HH", "aa", "CH"], move: 1 }],
      九: [{ regex: /九/g, visemes: ["kk", "U", "U"], move: 1 }],
      十: [{ regex: /十/g, visemes: ["jj", "U", "U"], move: 1 }],
      百: [{ regex: /百/g, visemes: ["HH", "jj", "aa", "kk", "U"], move: 1 }],
      千: [{ regex: /千/g, visemes: ["SS", "E", "nn"], move: 1 }],
      万: [{ regex: /万/g, visemes: ["PP", "aa", "nn"], move: 1 }],
      円: [{ regex: /円/g, visemes: ["E", "nn"], move: 1 }],
      日: [{ regex: /日/g, visemes: ["nn", "I", "CH"], move: 1 }],
      本: [{ regex: /本/g, visemes: ["HH", "OO", "nn"], move: 1 }],
      人: [{ regex: /人/g, visemes: ["nn", "I", "nn"], move: 1 }],
      年: [{ regex: /年/g, visemes: ["nn", "E", "nn"], move: 1 }],
      月: [{ regex: /月/g, visemes: ["kk", "E", "TS", "U"], move: 1 }],
      火: [{ regex: /火/g, visemes: ["kk", "aa"], move: 1 }],
      水: [{ regex: /水/g, visemes: ["PP", "I", "TS", "U"], move: 1 }],
      木: [{ regex: /木/g, visemes: ["kk", "I"], move: 1 }],
      金: [{ regex: /金/g, visemes: ["kk", "I", "nn"], move: 1 }],
      土: [{ regex: /土/g, visemes: ["TS", "U", "CH"], move: 1 }],
      上: [{ regex: /上/g, visemes: ["U", "E"], move: 1 }],
      下: [{ regex: /下/g, visemes: ["SS", "DD", "aa"], move: 1 }],
      中: [{ regex: /中/g, visemes: ["CH", "U", "U"], move: 1 }],
      大: [{ regex: /大/g, visemes: ["DD", "aa", "I"], move: 1 }],
      小: [
        { regex: /小/g, visemes: ["CH", "I", "I", "SS", "aa", "I"], move: 1 },
      ],
      山: [{ regex: /山/g, visemes: ["jj", "aa", "PP", "aa"], move: 1 }],
      川: [{ regex: /川/g, visemes: ["kk", "aa", "ww", "aa"], move: 1 }],
      学: [{ regex: /学/g, visemes: ["kk", "aa", "kk", "U"], move: 1 }],
      校: [{ regex: /校/g, visemes: ["kk", "OO"], move: 1 }],
      行: [{ regex: /行/g, visemes: ["I", "kk", "OO"], move: 1 }],
      来: [{ regex: /来/g, visemes: ["RR", "aa", "I"], move: 1 }],
      家: [{ regex: /家/g, visemes: ["I", "E"], move: 1 }],
      電: [{ regex: /電/g, visemes: ["DD", "E", "nn"], move: 1 }],
      車: [
        { regex: /車/g, visemes: ["kk", "U", "RR", "U", "PP", "aa"], move: 1 },
      ],
      食: [{ regex: /食/g, visemes: ["SS", "OO", "kk", "U"], move: 1 }],
      飲: [{ regex: /飲/g, visemes: ["I", "nn"], move: 1 }],
      読: [{ regex: /読/g, visemes: ["jj", "OO", "PP", "U"], move: 1 }],
      書: [{ regex: /書/g, visemes: ["kk", "aa", "kk"], move: 1 }],
      話: [{ regex: /話/g, visemes: ["ww", "aa"], move: 1 }],
      聞: [{ regex: /聞/g, visemes: ["kk", "I"], move: 1 }],
      見: [{ regex: /見/g, visemes: ["PP", "I"], move: 1 }],
      買: [{ regex: /買/g, visemes: ["kk", "aa", "I"], move: 1 }],
      売: [{ regex: /売/g, visemes: ["U", "RR"], move: 1 }],
      手: [{ regex: /手/g, visemes: ["DD", "E"], move: 1 }],
      足: [{ regex: /足/g, visemes: ["aa", "SS", "I"], move: 1 }],
      目: [{ regex: /目/g, visemes: ["PP", "E"], move: 1 }],
      口: [{ regex: /口/g, visemes: ["kk", "U", "CH"], move: 1 }],
      耳: [{ regex: /耳/g, visemes: ["PP", "I"], move: 1 }],
      頭: [{ regex: /頭/g, visemes: ["aa", "DD", "OO"], move: 1 }],
      心: [
        {
          regex: /心/g,
          visemes: ["kk", "OO", "kk", "OO", "RR", "OO"],
          move: 1,
        },
      ],
      時: [{ regex: /時/g, visemes: ["CH", "I"], move: 1 }],
      間: [{ regex: /間/g, visemes: ["kk", "aa", "nn"], move: 1 }],
      今: [{ regex: /今/g, visemes: ["I", "PP", "aa"], move: 1 }],
      何: [{ regex: /何/g, visemes: ["nn", "aa", "nn"], move: 1 }],
      名: [{ regex: /名/g, visemes: ["PP", "E", "I"], move: 1 }],
      前: [{ regex: /前/g, visemes: ["PP", "aa", "E"], move: 1 }],
      後: [{ regex: /後/g, visemes: ["aa", "DD"], move: 1 }],
      上: [{ regex: /上/g, visemes: ["U", "E"], move: 1 }],
      下: [{ regex: /下/g, visemes: ["SS", "I", "DD", "aa"], move: 1 }],
      左: [{ regex: /左/g, visemes: ["HH", "aa", "DD", "aa", "RR"], move: 1 }],
      右: [{ regex: /右/g, visemes: ["U"], move: 1 }],
      男: [{ regex: /男/g, visemes: ["OO", "DD", "OO", "kk", "OO"], move: 1 }],
      女: [{ regex: /女/g, visemes: ["OO", "nn", "nn", "aa"], move: 1 }],
      子: [{ regex: /子/g, visemes: ["kk", "OO"], move: 1 }],
      父: [{ regex: /父/g, visemes: ["CH", "I", "CH"], move: 1 }],
      母: [{ regex: /母/g, visemes: ["HH", "aa", "HH", "aa"], move: 1 }],
      友: [{ regex: /友/g, visemes: ["DD", "OO", "PP", "OO"], move: 1 }],
      達: [{ regex: /達/g, visemes: ["DD", "aa", "CH"], move: 1 }],
      先生: [
        { regex: /先生/g, visemes: ["SS", "E", "nn", "SS", "E", "I"], move: 1 },
      ],
      会社: [
        { regex: /会社/g, visemes: ["kk", "aa", "I", "SS", "aa"], move: 1 },
      ],
      学校: [{ regex: /学校/g, visemes: ["kk", "aa", "kk", "OO"], move: 1 }],
      病院: [
        { regex: /病院/g, visemes: ["PP", "jj", "OO", "I", "nn"], move: 1 },
      ],
      店: [{ regex: /店/g, visemes: ["PP", "I", "SS", "E"], move: 1 }],
      駅: [{ regex: /駅/g, visemes: ["E", "kk"], move: 1 }],
      車: [
        { regex: /車/g, visemes: ["kk", "U", "RR", "U", "PP", "aa"], move: 1 },
      ],
      本: [{ regex: /本/g, visemes: ["HH", "OO", "nn"], move: 1 }],
      食: [{ regex: /食/g, visemes: ["DD", "aa"], move: 1 }],
      水: [{ regex: /水/g, visemes: ["PP", "I", "TS", "U"], move: 1 }],
      電話: [
        { regex: /電話/g, visemes: ["DD", "E", "nn", "ww", "aa"], move: 1 },
      ],
      手紙: [
        { regex: /手紙/g, visemes: ["DD", "E", "kk", "aa", "PP"], move: 1 },
      ],
      時間: [
        { regex: /時間/g, visemes: ["CH", "I", "kk", "aa", "nn"], move: 1 },
      ],
      今日: [{ regex: /今日/g, visemes: ["kk", "jj", "OO"], move: 1 }],
      明日: [{ regex: /明日/g, visemes: ["aa", "SS", "U"], move: 1 }],
      昨日: [
        { regex: /昨日/g, visemes: ["kk", "I", "nn", "OO", "U"], move: 1 },
      ],
      朝: [{ regex: /朝/g, visemes: ["aa", "SS", "aa"], move: 1 }],
      昼: [{ regex: /昼/g, visemes: ["CH", "U", "U"], move: 1 }],
      夜: [{ regex: /夜/g, visemes: ["jj", "OO", "RR", "U"], move: 1 }],
      名前: [
        { regex: /名前/g, visemes: ["nn", "aa", "PP", "aa", "E"], move: 1 },
      ],
      仕事: [
        {
          regex: /仕事/g,
          visemes: ["SS", "I", "kk", "OO", "DD", "OO"],
          move: 1,
        },
      ],
      勉強: [
        {
          regex: /勉強/g,
          visemes: ["PP", "E", "nn", "kk", "jj", "OO"],
          move: 1,
        },
      ],
      旅行: [
        { regex: /旅行/g, visemes: ["RR", "jj", "OO", "kk", "OO"], move: 1 },
      ],
      食事: [{ regex: /食事/g, visemes: ["SS", "OO", "kk", "CH"], move: 1 }],
      買い物: [
        {
          regex: /買い物/g,
          visemes: ["kk", "aa", "I", "PP", "OO", "nn"],
          move: 1,
        },
      ],
      料理: [{ regex: /料理/g, visemes: ["RR", "jj", "OO", "RR"], move: 1 }],
      音楽: [
        {
          regex: /音楽/g,
          visemes: ["OO", "nn", "kk", "aa", "kk", "U"],
          move: 1,
        },
      ],
      映画: [{ regex: /映画/g, visemes: ["E", "I", "kk", "aa"], move: 1 }],
      天気: [
        { regex: /天気/g, visemes: ["DD", "E", "nn", "kk", "I"], move: 1 },
      ],
      時間: [
        { regex: /時間/g, visemes: ["CH", "I", "kk", "aa", "nn"], move: 1 },
      ],
      場所: [{ regex: /場所/g, visemes: ["jj", "aa", "SS", "OO"], move: 1 }],
      問題: [
        {
          regex: /問題/g,
          visemes: ["PP", "OO", "nn", "DD", "aa", "I"],
          move: 1,
        },
      ],
      大丈夫: [
        {
          regex: /大丈夫/g,
          visemes: ["DD", "aa", "I", "CH", "OO", "U", "PP", "U"],
          move: 1,
        },
      ],
      ありがとう: [
        {
          regex: /ありがとう/g,
          visemes: ["aa", "RR", "I", "kk", "aa", "DD", "OO", "U"],
          move: 1,
        },
      ],
      すみません: [
        {
          regex: /すみません/g,
          visemes: ["SS", "U", "PP", "I", "PP", "aa", "SS", "E", "nn"],
          move: 1,
        },
      ],
      わかりません: [
        {
          regex: /わかりません/g,
          visemes: [
            "ww",
            "aa",
            "kk",
            "aa",
            "RR",
            "I",
            "PP",
            "aa",
            "SS",
            "E",
            "nn",
          ],
          move: 1,
        },
      ],
      お願いします: [
        {
          regex: /お願いします/g,
          visemes: [
            "OO",
            "nn",
            "E",
            "kk",
            "aa",
            "I",
            "SS",
            "I",
            "PP",
            "aa",
            "SS",
            "U",
          ],
          move: 1,
        },
      ],
    };

    // Viseme durations in relative unit (1=average)
    this.visemeDurations = {
      aa: 0.95,
      E: 0.9,
      I: 0.92,
      O: 0.96,
      U: 0.95,
      PP: 1.08,
      SS: 1.23,
      TH: 1,
      DD: 1.05,
      FF: 1.0,
      kk: 1.21,
      nn: 0.88,
      RR: 0.88,
      CH: 1.0,
      TS: 1.0,
      HH: 1.0,
      jj: 1.0,
      ww: 1.0,
      OO: 1.0,
      sil: 1,
    };

    // Pauses in relative units (1=average)
    this.specialDurations = { " ": 1, ",": 3, "-": 0.5, "'": 0.5 };

    // Japanese number words
    this.digits = [
      "ゼロ",
      "いち",
      "に",
      "さん",
      "よん",
      "ご",
      "ろく",
      "なな",
      "はち",
      "きゅう",
    ];
    this.powersOfTen = ["", "十", "百", "千", "万", "億", "兆", "京"];

    // Symbols to Japanese
    this.symbols = {
      "%": "パーセント",
      "€": "ユーロ",
      "&": "アンド",
      "+": "プラス",
      $: "ドル",
    };
    this.symbolsReg = /[%€&\+\$]/g;
  }

  /**
   * Preprocess text:
   * - convert symbols to words
   * - convert numbers to words
   * - filter out characters that should be left unspoken
   * @param  s Text
   * @return  Pre-processsed text.
   */
  preProcessText(s) {
    s = s.replace(/[\s]+/g, " ");
    // Convert symbols to words
    s = s.replace(this.symbolsReg, (symbol) => {
      return " " + this.symbols[symbol] + " ";
    });

    // Convert numbers to words
    const numberRegEx = /(\d+)/g;
    s = s.replace(numberRegEx, (match) => {
      return this.convertNumberToWords(match) + " ";
    });

    // Add spaces around punctuation to separate words
    s = s.replace(/([、。！？])/g, " $1 ");

    return s.trim();
  }

  /**
   * Convert word to Oculus LipSync Visemes and durations
   * @param  w Text
   * @return  Oculus LipSync Visemes and durations.
   */
  wordsToVisemes(w) {
    let o = { words: w, visemes: [], times: [], durations: [], i: 0 };
    let t = 0;

    const chars = [...o.words];
    while (o.i < chars.length) {
      const c = chars[o.i];
      const ruleset = this.rules[c];

      if (ruleset) {
        for (let i = 0; i < ruleset.length; i++) {
          const rule = ruleset[i];
          const test = o.words.substring(o.i);
          let matches = test.match(rule.regex);

          if (matches) {
            rule.visemes.forEach((viseme) => {
              if (
                o.visemes.length &&
                o.visemes[o.visemes.length - 1] === viseme
              ) {
                const d = 0.7 * (this.visemeDurations[viseme] || 1);
                o.durations[o.durations.length - 1] += d;
                t += d;
              } else {
                const d = this.visemeDurations[viseme] || 1;
                o.visemes.push(viseme);
                o.times.push(t);
                o.durations.push(d);
                t += d;
              }
            });
            o.i += rule.move;
            break;
          }
        }
      } else {
        o.i++;
        t += this.specialDurations[c] || 0;
      }
    }

    return o;
  }

  /**
   * Convert a number to Japanese words
   * @param {number} num
   * @returns {string} The number in Japanese words
   */
  convertNumberToWords(num) {
    if (num === 0) {
      return this.digits[0];
    }

    if (num >= 10000) {
      let result = "";
      for (let i = Math.floor(Math.log10(num) / 4) * 4; i >= 0; i -= 4) {
        const chunk = Math.floor(num / Math.pow(10, i)) % 10000;
        if (chunk > 0) {
          result +=
            this.convertFourDigitNumber(chunk) + this.powersOfTen[i / 4];
        }
      }
      return result;
    } else {
      return this.convertFourDigitNumber(num);
    }
  }

  /**
   * Convert a four-digit number to Japanese words
   * @param {number} num
   * @returns {string} The four-digit number in Japanese words
   */
  convertFourDigitNumber(num) {
    if (num === 0) {
      return "";
    }

    let result = "";
    if (num >= 1000) {
      result += this.digits[Math.floor(num / 1000)] + this.powersOfTen[3];
      num %= 1000;
    }

    if (num >= 100) {
      result += this.digits[Math.floor(num / 100)] + this.powersOfTen[2];
      num %= 100;
    }

    if (num >= 10) {
      result += this.digits[Math.floor(num / 10)] + this.powersOfTen[1];
      num %= 10;
    }

    if (num > 0) {
      result += this.digits[num];
    }

    return result;
  }
}

export { LipsyncJa };
