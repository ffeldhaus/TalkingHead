class LipsyncAr {

  constructor() {
    this.visemeMappings = {
      'ا': 'aa', 'ى': 'aa', 'آ': 'aa', 'أ': 'aa', 'إ': 'aa',
      'و': 'O', 'ؤ': 'O',
      'ي': 'I', 'ې': 'I', 'ئ': 'I', 'ى': 'I',
      'ب': 'PP', 'پ': 'PP', 'م': 'PP',
      'ف': 'FF',
      'ت': 'DD', 'ط': 'DD', 'د': 'DD', 'ض': 'DD', 'ذ': 'TH', 'ظ': 'TH', 'ث': 'TH',
      'ك': 'kk', 'ق': 'kk', 'غ': 'kk', 'ج': 'kk', 'خ': 'kk', 'ه': 'kk', 'ح': 'kk', 'ع': 'kk',
      'س': 'SS', 'ص': 'SS', 'ز': 'SS', 'ش': 'SS', 'ژ': 'SS',
      'ر': 'RR', 'ل': 'nn', 'ن': 'nn',
      ' ': 'sil'
    };

    this.visemeDurations = {
      'aa': 0.95, 'E': 0.90, 'I': 0.92, 'O': 0.96, 'U': 0.95, 'PP': 1.08,
      'SS': 1.23, 'TH': 1, 'DD': 1.05, 'FF': 1.00, 'kk': 1.21, 'nn': 0.88,
      'RR': 0.88, 'sil': 1
    };

        this.specialDurations = { ' ': 1, ',': 3, '-':0.5, "'":0.5 };
  }

  preProcessText(s) {
    return s.replace(/[#,.;"؟،؛]/g, '');
  }

  wordsToVisemes(w) {
    let visemes = [];
    let times = [];
    let durations = [];
    let t = 0;

    for (let i = 0; i < w.length; i++) {        
      const char = w[i];
      console.log("char", char)
      const viseme = this.visemeMappings[char];

      if (viseme) {
        if (visemes.length > 0 && visemes[visemes.length - 1] === viseme) {
          const d = 0.7 * (this.visemeDurations[viseme] || 1);
          durations[durations.length - 1] += d;
          t += d;
        } else {
          const d = this.visemeDurations[viseme] || 1;
          visemes.push(viseme);
          times.push(t);
          durations.push(d);
          t += d;
        }
      } else {
          t += this.specialDurations[char] || 0;
      }
    }

    return { visemes, times, durations };
  }
}

export { LipsyncAr };