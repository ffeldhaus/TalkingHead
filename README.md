# GenAI Avatar

## Introduction

Talking Head (3D) is a JavaScript class featuring a 3D avatar that can
speak and lip-sync in real-time. The class supports
[Ready Player Me](https://readyplayer.me/) full-body 3D avatars (GLB),
[Mixamo](https://www.mixamo.com) animations (FBX), and subtitles.
It also knows a set of emojis, which it can convert into facial expressions.

It uses
[Google Cloud TTS](https://cloud.google.com/text-to-speech) for text-to-speech
and has a built-in lip-sync support for English, Finnish, and Lithuanian (beta).
New lip-sync languages can be added by creating new lip-sync language modules.

The class uses [ThreeJS](https://github.com/mrdoob/three.js/) / WebGL for 3D
rendering.

The solution is based on [Talking Head (3D)](https://github.com/met4citizen/TalkingHead) but significantly enhanced with Google Cloud GenAI features.

## Supported Browsers

Only the Chrome browser is currently tested, other Chromium based browsers likely will work as well. Firefox is not supported due to missing [`grabFrame`](https://developer.mozilla.org/en-US/docs/Web/API/ImageCapture/grabFrame) support. Safari is not supported due to many missing features.