// Site configuration
export const site = {

  // Preset avatars
  avatars: {
    'GCP': {
      url: './avatars/GCP.glb',
      body: 'M',
      avatarMood: 'happy',
      fi: 'GCP'
    },
    'fan': {
      url: './avatars/fan.glb',
      body: 'M',
      avatarMood: 'happy',
      fi: 'fan'
    },
    'Brunette': {
      url: './avatars/brunette.glb',
      body: 'F',
      avatarMood: 'neutral',
      fi: 'Brunetti'
    }
  },

  // Google voices
  googleVoices: {
    "fi-F": { id: "fi-FI-Standard-A" },
    "lv-M": { id: "lv-LV-Standard-A" },
    "lt-M": { id: "lt-LT-Standard-A" },
    "en-F": { id: "en-GB-Standard-A" },
    "en-M": { id: "en-GB-Standard-D" }
  },

  // Preset views
  views: {
    'DrStrange': { url: './views/strange.jpg', type: 'image/jpg', fi: 'TohtoriOuto' },
    'EM24': { url: './views/em24.jpg', type: 'image/jpg', fi: 'EM24' },
    'Matrix': { url: './views/matrix.mp4', type: 'video/mp4' }
  },

  // Preset poses (includes internal poses)
  poses: {
    'Straight': { url: "straight", fi: 'Suora' },
    'Side': { url: "side", fi: 'Keno' },
    'Hip': { url: "hip", fi: 'Lantio' },
    'Turn': { url: "turn", fi: 'Sivu' },
    'Back': { url: "back", fi: 'Taka' },
    'Wide': { url: "wide", fi: 'Haara' },
    'OneKnee': { url: "oneknee", fi: 'Polvi' },
    'TwoKnees': { url: "kneel", fi: 'Polvet' },
    'Bend': { url: "bend", fi: 'Perä' },
    'Sitting': { url:"sitting", fi: 'Istuva' },
    'Dance': { url: './poses/dance.fbx', fi: 'Tanssi' }
  },

  // Preset gestures
  gestures: {
    'HandUp': { name: "handup", fi: 'KäsiYlös' },
    'OK': { name: "ok" },
    'Index': { name: "index", fi: 'Etusormi' },
    'ThumbUp': { name: "thumbup", fi: 'PeukaloYlös' },
    'ThumbDown': { name: "thumbdown", fi: 'PeukaloAlas' },
    'Side': { name: "side", fi: 'Sivu' },
    'Shrug': { name: "shrug", fi: 'Olankohautus' },
    'Namaste': { name: "namaste" }
  },

  // Preset animations
  animations: {
    'Walking': { url: './animations/walking.fbx', fi: 'Kävely' }
  },

  // Impulse responses
  impulses: {
    'Room': { url: './audio/ir-room.m4a', fi: 'Huone' },
    'Basement': { url: './audio/ir-basement.m4a', fi: 'Kellari' },
    'Forest': { url: './audio/ir-forest.m4a', fi: 'Metsä' },
    'Church': { url: './audio/ir-church.m4a', fi: 'Kirkko' },
    'Stairwell': { url: './audio/ir-stairwell.m4a', fi: 'Portaikko' },
    'Auditorium': { url: './audio/ir-auditorium.m4a', fi: 'Auditorio' }
  },

  // Background ambient sounds/music
  music: {
    'Murmur': { url: './audio/murmur.mp3', fi: 'Puheensorina'}
  }

};
